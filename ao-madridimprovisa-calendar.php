<?php 
/*
Plugin Name: Madrid Improvisa Calendar
*/
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

require_once("ao-mi-calendar_widget.php");

class AO_MadridImprovisa_Calendar {

  var $prefix_metadata = "_aomi_event_";


  /**
   * __construct function.
   *
   * @access public
   * @return void
   */
  public function __construct() {
    // EVENTS
    add_action( 'init', array( $this, 'create_post_type' ) );

    add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
    add_action( 'save_post', array( $this, 'save_post' ), 1, 2 );
    add_action( 'aomi_calendar_save_event', array( $this, 'save_event_data' ), 20, 2 );

    // CALENDAR
    add_shortcode( 'aomi_calendar', array( $this, 'calendar') );
    add_action( 'admin_menu', array( $this, 'admin_menu') );

    // WIDGET
    add_action( 'widgets_init', function(){
      register_widget( 'aomi_calendarwidget' );
    });
  }



  function admin_menu() {
    //add_menu_page( 'My Top Level Menu Example', 'Top Level Menu', 'manage_options', 'myplugin/myplugin-admin-page.php', 'myplguin_admin_page', 'dashicons-tickets', 6  );
    add_submenu_page( 'edit.php?post_type=aomi_event', 'Calendario', 'Calendario', 'manage_options',  'aomi_calendar', array($this, 'admin_calendar') ); 
  }


  /**
   * create_post_type function.
   *
   * @access public
   * @return void
   */
  function create_post_type() {
    register_post_type( 'aomi_event',
      array(
        'labels' => array(
          'name' => __( 'Eventos' ),
          'singular_name' => __( 'Evento' )
        ),
        'public' => true,
        'has_archive' => false,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-tickets-alt',
        'supports' => array(
          'title','custom-fields'
        ),
        'rewrite' => array('slug' => 'event'),
      )
    );
  }

  /**
   * event_fields function.
   *
   * @access public
   * @return void
   */
  public function event_fields() {
    global $post;

    $current_user = wp_get_current_user();

/*
 "type"          => "textarea",
                "scope"         =>   array( "page" ),
                "capability"    => "edit_pages"

        'value'       => metadata_exists( 'post', $post->ID, '_application' ) ? get_post_meta( $post->ID, '_application', true ) : $current_user->user_email,

*/

    $fields = array(      
      $this->prefix_metadata.'show_id' => array(
        'label' => __( 'Show ID', 'ao-madridimprovisa' ),
        'priority'    => 0,
        'linked'  => 'page'
      ),
      $this->prefix_metadata.'tagline' => array(
        'label' => __( 'Tagline', 'ao-madridimprovisa' ),
        'placeholder' => __( 'p.e. "Infantil"', 'ao-madridimprovisa' ),
        'priority'    => 1,
      ),
      $this->prefix_metadata.'company' => array(
        'label' => __( 'Compañía', 'ao-madridimprovisa' ),
        'priority'    => 2,
        'preview'     => true,
      ),
      $this->prefix_metadata.'company_id' => array(
        'label' => __( 'ID Compañía', 'ao-madridimprovisa' ),
        'priority'    => 3,
        'linked'  => 'page'
      ),
      $this->prefix_metadata.'venue' => array(
        'label' => __( 'Sala', 'ao-madridimprovisa' ),
        'priority'    => 10,
        'preview'     => true,
      ),
      $this->prefix_metadata.'price' => array(
        'label' => __( 'Precio', 'ao-madridimprovisa' ),
        'priority'    => 20,
      ),
      $this->prefix_metadata.'tickets' => array(
        'label' => __( 'Entradas', 'ao-madridimprovisa' ),
        'priority'    => 21,
        'preview'     => true,
      ),
      $this->prefix_metadata.'social' => array(
        'label' => __( 'Enlaces sociales', 'ao-madridimprovisa' ),
        'priority'    => 30,
        'preview'     => true,
      ),
      /*    
      '_datetime' => array(
        'label' => __( 'Fecha', 'ao-madridimprovisa' ),
        'placeholder' => _x( 'yyyy-mm-dd', 'Date format placeholder', 'ao-madridimprovisa' ),
      //  'description' => __( '', 'ao-madridimprovisa' ),
        'priority'    => 6
      ),
      */
    );

    $fields = apply_filters( 'aomi_event_data_fields', $fields );

    uasort( $fields, array( $this, 'sort_by_priority' ) );

    return $fields;
  }

  /**
   * Sort array by priority value
   */
  protected function sort_by_priority( $a, $b ) {
      if ( ! isset( $a['priority'] ) || ! isset( $b['priority'] ) || $a['priority'] === $b['priority'] ) {
          return 0;
      }
      return ( $a['priority'] < $b['priority'] ) ? -1 : 1;
  }

  /**
   * add_meta_boxes function.
   *
   * @access public
   * @return void
   */
  public function add_meta_boxes() {
    global $wp_post_types;

    add_meta_box( 'event_data',
      sprintf( __( '%s MetaData', 'ao-madridimprovisa' ), $wp_post_types['aomi_event']->labels->singular_name ),
      array( $this, 'event_data' ),
      'aomi_event', 'normal', 'high' );
  }

  /**
   * event_data function.
   *
   * @access public
   * @param mixed $post
   * @return void
   */
  public function event_data( $post ) {
    global $post, $thepostid;

    $thepostid = $post->ID;

    echo '<div class="ao_madridimprovisa_event_meta_data">';

    wp_nonce_field( 'save_meta_data', 'aomi_event_nonce' );

    do_action( 'aomi_event_data_start', $thepostid );

    foreach ( $this->event_fields() as $key => $field ) {
      $type = ! empty( $field['type'] ) ? $field['type'] : 'text';

      if ( has_action( 'aomi_input_' . $type ) ) {
        do_action( 'aomi_input_' . $type, $key, $field );
      } elseif ( method_exists( $this, 'input_' . $type ) ) {
        call_user_func( array( $this, 'input_' . $type ), $key, $field );
      }

    }

    do_action( 'aomi_event_data_end', $thepostid );

    echo '</div>';
  }

  /**
   * save_post function.
   *
   * @access public
   * @param mixed $post_id
   * @param mixed $post
   * @return void
   */
  public function save_post( $post_id, $post ) {
    if ( empty( $post_id ) || empty( $post ) || empty( $_POST ) ) return;
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return;
    if ( is_int( wp_is_post_revision( $post ) ) ) return;
    if ( is_int( wp_is_post_autosave( $post ) ) ) return;
    if ( empty($_POST['aomi_event_nonce']) || ! wp_verify_nonce( $_POST['aomi_event_nonce'], 'save_meta_data' ) ) return;
    if ( ! current_user_can( 'edit_post', $post_id ) ) return;
    if ( $post->post_type != 'aomi_event' ) return;

    do_action( 'aomi_calendar_save_event', $post_id, $post );
  }
  
  /**
   * save_event_data function.
   *
   * @access public
   * @param mixed $post_id
   * @param mixed $post
   * @return void
   */
  public function save_event_data( $post_id, $post ) {
    global $wpdb;

    // These need to exist
    //add_post_meta( $post_id, '_filled', 0, true );
    //add_post_meta( $post_id, '_featured', 0, true );

    // Save fields
    foreach ( $this->event_fields() as $key => $field ) {

      // Locations
      /*
      elseif ( '_job_location' === $key ) {
        if ( update_post_meta( $post_id, $key, sanitize_text_field( $_POST[ $key ] ) ) ) {
          // Location data will be updated by hooked in methods
        } elseif ( apply_filters( 'job_manager_geolocation_enabled', true ) && ! WP_Job_Manager_Geocode::has_location_data( $post_id ) ) {
          WP_Job_Manager_Geocode::generate_location_data( $post_id, sanitize_text_field( $_POST[ $key ] ) );
        }
      }
      */

      /*
      elseif ( '_job_author' === $key ) {
        $wpdb->update( $wpdb->posts, array( 'post_author' => $_POST[ $key ] > 0 ? absint( $_POST[ $key ] ) : 0 ), array( 'ID' => $post_id ) );
      }

      elseif ( '_application' === $key ) {
        update_post_meta( $post_id, $key, sanitize_text_field( urldecode( $_POST[ $key ] ) ) );
      }
      */

      // Everything else
      //else {

      $type = ! empty( $field['type'] ) ? $field['type'] : '';

      switch ( $type ) {
        case 'textarea' :
          update_post_meta( $post_id, $key, wp_kses_post( stripslashes( $_POST[ $key ] ) ) );
        break;
        case 'checkbox' :
          if ( isset( $_POST[ $key ] ) ) {
            update_post_meta( $post_id, $key, 1 );
          } else {
            update_post_meta( $post_id, $key, 0 );
          }
        break;
        default :
          if ( ! isset( $_POST[ $key ] ) ) {
            continue;
          } elseif ( is_array( $_POST[ $key ] ) ) {
            update_post_meta( $post_id, $key, array_filter( array_map( 'sanitize_text_field', $_POST[ $key ] ) ) );
          } else {
            //update_post_meta( $post_id, $key, sanitize_text_field( $_POST[ $key ] ) );
            update_post_meta( $post_id, $key, $_POST[ $key ] );
          }
        break;
      }
      //}
    }
  }


  // ---------------
    /**
   * input_file function.
   *
   * @param mixed $key
   * @param mixed $field
   */
  public static function input_file( $key, $field ) {
    global $thepostid;

    if ( ! isset( $field['value'] ) ) {
      $field['value'] = get_post_meta( $thepostid, $key, true );
    }
    if ( empty( $field['placeholder'] ) ) {
      $field['placeholder'] = 'http://';
    }
    if ( ! empty( $field['name'] ) ) {
      $name = $field['name'];
    } else {
      $name = $key;
    }
    ?>
    <p class="form-field">
      <label for="<?php echo esc_attr( $key ); ?>"><?php echo esc_html( $field['label'] ) ; ?>: <?php if ( ! empty( $field['description'] ) ) : ?><span class="tips" data-tip="<?php echo esc_attr( $field['description'] ); ?>">[?]</span><?php endif; ?></label>
      <?php
      if ( ! empty( $field['multiple'] ) ) {
        foreach ( (array) $field['value'] as $value ) {
          ?><span class="file_url"><input type="text" name="<?php echo esc_attr( $name ); ?>[]" placeholder="<?php echo esc_attr( $field['placeholder'] ); ?>" value="<?php echo esc_attr( $value ); ?>" /><button class="button button-small wp_job_manager_upload_file_button" data-uploader_button_text="<?php _e( 'Use file', 'wp-job-manager' ); ?>"><?php _e( 'Upload', 'wp-job-manager' ); ?></button></span><?php
        }
      } else {
        ?><span class="file_url"><input type="text" name="<?php echo esc_attr( $name ); ?>" id="<?php echo esc_attr( $key ); ?>" placeholder="<?php echo esc_attr( $field['placeholder'] ); ?>" value="<?php echo esc_attr( $field['value'] ); ?>" /><button class="button button-small wp_job_manager_upload_file_button" data-uploader_button_text="<?php _e( 'Use file', 'wp-job-manager' ); ?>"><?php _e( 'Upload', 'wp-job-manager' ); ?></button></span><?php
      }
      if ( ! empty( $field['multiple'] ) ) {
        ?><button class="button button-small wp_job_manager_add_another_file_button" data-field_name="<?php echo esc_attr( $key ); ?>" data-field_placeholder="<?php echo esc_attr( $field['placeholder'] ); ?>" data-uploader_button_text="<?php _e( 'Use file', 'wp-job-manager' ); ?>" data-uploader_button="<?php _e( 'Upload', 'wp-job-manager' ); ?>"><?php _e( 'Add file', 'wp-job-manager' ); ?></button><?php
      }
      ?>
    </p>
    <?php
  }

  /**
   * input_text function.
   *
   * @param mixed $key
   * @param mixed $field
   */
  public static function input_text( $key, $field ) {
    global $thepostid;

    if ( ! isset( $field['value'] ) ) {
      $field['value'] = get_post_meta( $thepostid, $key, true );
    }
    if ( ! empty( $field['name'] ) ) {
      $name = $field['name'];
    } else {
      $name = $key;
    }
    if ( ! empty( $field['classes'] ) ) {
      $classes = implode( ' ', is_array( $field['classes'] ) ? $field['classes'] : array( $field['classes'] ) );
    } else {
      $classes = '';
    }
    ?>
    <p class="form-field">
      <label for="<?php echo esc_attr( $key ); ?>"><?php echo esc_html( $field['label'] ) ; ?>: <?php if ( ! empty( $field['description'] ) ) : ?><span class="tips" data-tip="<?php echo esc_attr( $field['description'] ); ?>">[?]</span><?php endif; ?></label>
      <input type="text" name="<?php echo esc_attr( $name ); ?>" class="<?php echo esc_attr( $classes ); ?>" id="<?php echo esc_attr( $key ); ?>" placeholder="<?php echo esc_attr( $field['placeholder'] ); ?>"
        value="<?php echo esc_attr($field['value']); ?>" />
      <?php
      if($field['value'] != "" && isset($field['preview']) && $field['preview']){
        echo do_shortcode( $field['value'])."<br/>\n";
      }
      if($field['value'] != "" && isset($field['linked']) && $field['linked']){
        switch (variable) {
          case 'company':
            break;
          default:
            $post = get_post( (int)$field['value'] );
            break;
        }
        if($post)
          echo '<a href="'.get_permalink( $post ).'">'.get_the_title($post).'</a>'."<br/>\n";
        else
          echo "NO POST<br/>\n";
      }
      ?>
    </p>
    <?php
/*
      <input type="text" name="<?php echo esc_attr( $name ); ?>" class="<?php echo esc_attr( $classes ); ?>" id="<?php echo esc_attr( $key ); ?>" placeholder="<?php echo esc_attr( $field['placeholder'] ); ?>"
        value="<?php echo esc_attr( $field['value'] ); ?>" />
*/   
  }

  /**
   * input_text function.
   *
   * @param mixed $key
   * @param mixed $field
   */
  public static function input_textarea( $key, $field ) {
    global $thepostid;

    if ( ! isset( $field['value'] ) ) {
      $field['value'] = get_post_meta( $thepostid, $key, true );
    }
    if ( ! empty( $field['name'] ) ) {
      $name = $field['name'];
    } else {
      $name = $key;
    }
    ?>
    <p class="form-field">
      <label for="<?php echo esc_attr( $key ); ?>"><?php echo esc_html( $field['label'] ) ; ?>: <?php if ( ! empty( $field['description'] ) ) : ?><span class="tips" data-tip="<?php echo esc_attr( $field['description'] ); ?>">[?]</span><?php endif; ?></label>
      <textarea name="<?php echo esc_attr( $name ); ?>" id="<?php echo esc_attr( $key ); ?>" placeholder="<?php echo esc_attr( $field['placeholder'] ); ?>"><?php echo esc_html( $field['value'] ); ?></textarea>
    </p>
    <?php
  }

  /**
   * input_select function.
   *
   * @param mixed $key
   * @param mixed $field
   */
  public static function input_select( $key, $field ) {
    global $thepostid;

    if ( ! isset( $field['value'] ) ) {
      $field['value'] = get_post_meta( $thepostid, $key, true );
    }
    if ( ! empty( $field['name'] ) ) {
      $name = $field['name'];
    } else {
      $name = $key;
    }
    ?>
    <p class="form-field">
      <label for="<?php echo esc_attr( $key ); ?>"><?php echo esc_html( $field['label'] ) ; ?>: <?php if ( ! empty( $field['description'] ) ) : ?><span class="tips" data-tip="<?php echo esc_attr( $field['description'] ); ?>">[?]</span><?php endif; ?></label>
      <select name="<?php echo esc_attr( $name ); ?>" id="<?php echo esc_attr( $key ); ?>">
        <?php foreach ( $field['options'] as $key => $value ) : ?>
        <option value="<?php echo esc_attr( $key ); ?>" <?php if ( isset( $field['value'] ) ) selected( $field['value'], $key ); ?>><?php echo esc_html( $value ); ?></option>
        <?php endforeach; ?>
      </select>
    </p>
    <?php
  }

  /**
   * input_select function.
   *
   * @param mixed $key
   * @param mixed $field
   */
  public static function input_multiselect( $key, $field ) {
    global $thepostid;

    if ( ! isset( $field['value'] ) ) {
      $field['value'] = get_post_meta( $thepostid, $key, true );
    }
    if ( ! empty( $field['name'] ) ) {
      $name = $field['name'];
    } else {
      $name = $key;
    }
    ?>
    <p class="form-field">
      <label for="<?php echo esc_attr( $key ); ?>"><?php echo esc_html( $field['label'] ) ; ?>: <?php if ( ! empty( $field['description'] ) ) : ?><span class="tips" data-tip="<?php echo esc_attr( $field['description'] ); ?>">[?]</span><?php endif; ?></label>
      <select multiple="multiple" name="<?php echo esc_attr( $name ); ?>[]" id="<?php echo esc_attr( $key ); ?>">
        <?php foreach ( $field['options'] as $key => $value ) : ?>
        <option value="<?php echo esc_attr( $key ); ?>" <?php if ( ! empty( $field['value'] ) && is_array( $field['value'] ) ) selected( in_array( $key, $field['value'] ), true ); ?>><?php echo esc_html( $value ); ?></option>
        <?php endforeach; ?>
      </select>
    </p>
    <?php
  }

  /**
   * input_checkbox function.
   *
   * @param mixed $key
   * @param mixed $field
   */
  public static function input_checkbox( $key, $field ) {
    global $thepostid;

    if ( empty( $field['value'] ) ) {
      $field['value'] = get_post_meta( $thepostid, $key, true );
    }
    if ( ! empty( $field['name'] ) ) {
      $name = $field['name'];
    } else {
      $name = $key;
    }
    ?>
    <p class="form-field form-field-checkbox">
      <label for="<?php echo esc_attr( $key ); ?>"><?php echo esc_html( $field['label'] ) ; ?></label>
      <input type="checkbox" class="checkbox" name="<?php echo esc_attr( $name ); ?>" id="<?php echo esc_attr( $key ); ?>" value="1" <?php checked( $field['value'], 1 ); ?> />
      <?php if ( ! empty( $field['description'] ) ) : ?><span class="description"><?php echo $field['description']; ?></span><?php endif; ?>
    </p>
    <?php
  }

  /**
   * input_radio function.
   *
   * @param mixed $key
   * @param mixed $field
   */
  public static function input_radio( $key, $field ) {
    global $thepostid;

    if ( empty( $field['value'] ) ) {
      $field['value'] = get_post_meta( $thepostid, $key, true );
    }
    if ( ! empty( $field['name'] ) ) {
      $name = $field['name'];
    } else {
      $name = $key;
    }
    ?>
    <p class="form-field form-field-checkbox">
      <label><?php echo esc_html( $field['label'] ) ; ?></label>
      <?php foreach ( $field['options'] as $option_key => $value ) : ?>
        <label><input type="radio" class="radio" name="<?php echo esc_attr( isset( $field['name'] ) ? $field['name'] : $key ); ?>" value="<?php echo esc_attr( $option_key ); ?>" <?php checked( $field['value'], $option_key ); ?> /> <?php echo esc_html( $value ); ?></label>
      <?php endforeach; ?>
      <?php if ( ! empty( $field['description'] ) ) : ?><span class="description"><?php echo $field['description']; ?></span><?php endif; ?>
    </p>
    <?php
  }



  function get_calendar_events($args = array()){
    
    $default_args = array(
      'post_type' => 'aomi_event',
      'posts_per_page' => -1,
//      'post_status' => 'any',
      'post_status' => array( 'publish', 'future' ),
      
      'date_query'     => array(
        array(
          'after' => 'today + 3 hours',
          'before' => 'tomorrow +3 hours'
        )
      ),
/*
      'meta_query'  => array(
         array(
          'key'     => 'event-date',
          'value'   => array($today,$future),
          'compare' => 'BETWEEN',
          'type'    => 'DATE'
        )
       ),
*/
      'orderby' => 'date',
      'order'   => 'ASC',
      );

    if(isset($args['company_id'])){
      $args['meta_query'] = array(
        array(
          'key'     => $this->prefix_metadata.'company_id',
          'value'  => $args['company_id'],
          'compare' => '=',
          )
        );
      $args['date_query'] = array(
        array(
          'after' => 'today + 3 hours',
          'before' => '3 months'
          )
        );

      unset($args['company_id']);
    }

    if(isset($args['show_id'])){
      $args['meta_query'] = array(
        array(
          'key'     => $this->prefix_metadata.'show_id',
          'value'  => $args['show_id'],
          'compare' => '=',
          )
        );
      $args['date_query'] = array(
        array(
          'after' => 'today + 3 hours',
          'before' => '3 months'
          )
        );

      unset($args['show_id']);
    }

    $args = wp_parse_args($args,$default_args);

    $loop = new WP_Query( $args );

    return $loop;
  }


  function calendar($atts){
    global $post;
    
    $screen = get_queried_object();
    //$text = "Calendar: ".$screen->ID;

    $atts = shortcode_atts( array(
      0 => null,
      'company' => null,
      'show' => null,
    ), $atts );

    if($atts[0] != null)
      $atts['type'] = $atts[0];
    
    // var_dump($atts);

    switch ($atts['type']) {
      case 'show':
        $text .= "<h4>Próximas funciones</h4>\n";
        $text .= "<!-- SHOW ".$screen->ID." -->\n";
        $text .= $this->calendar_show($screen->ID);
        break;
      case 'company':
        $text .= "<h4>Próximas funciones</h4>\n";
        $text .= "<!-- COMPANY ".$screen->ID." -->\n";
        $text .= $this->calendar_company($screen->ID);
        break;      
      case 'full':
      default:
        $text .= "<!-- FULL CALENDAR -->\n";
        $text .= $this->calendar_full();
        break;
    }

    return $text;
  }

  function calendar_full(){

    $args['date_query'] = array(
        'after' => 'today + 3 hours',
        'before' => '1 week + 3 hours'
      );

    $loop = $this->get_calendar_events($args);

    if($loop)
       return $this->show_calendar($loop);

    return "";
  }

  function calendar_company($id=null){
    $loop = $this->get_calendar_events(
      array(
        'company_id' => $id
        )
      );
    //var_dump($loop);
    return $this->show_calendar($loop);
  }

  function calendar_show($id=null){
    $loop = $this->get_calendar_events(
      array(
        'show_id' => $id
        )
      );
    //var_dump($loop);
    return $this->show_calendar($loop);
  }

  function show_calendar($loop){
    
    ob_start();
    ?>
    <div class="agenda">      
    <?php
    $day = "";
    while ( $loop->have_posts() ) : $loop->the_post();   

      $t = get_post_time() - 3*3600; /* Hasta las 3AM es el mismo día */
      $tday  = date('dmy', $t);
      if($day != $tday) {
        if ($day !="")
          echo "</ul>";
        $day = $tday;
        ?> 
          <h3><?= date_i18n('l j F',$t); ?></h3>
          <ul>
       <?php
      }

      $postmeta = get_post_meta( get_the_ID() );
      //var_dump($postmeta); 

      ?>
      <li><span class="hora"><?php the_time('G:i\h'); ?></span>
        <span class="titulo"><?php
        if($postmeta[$this->prefix_metadata.'show_id'][0] != ''){
          ?><a href="<?= get_permalink( $postmeta[$this->prefix_metadata.'show_id'][0] ) ?>"><?php the_title(); ?></a><?php
        } else {
         the_title();
        }

        if(current_user_can('edit_post')){
          ?> <a href="<?= get_edit_post_link( get_the_ID() ) ?>" class="icon icon-edit">EDIT</a><?php
        }
        ?></span><span
         class="precio"><?= $postmeta[$this->prefix_metadata.'price'][0] ?></span><span
         class="compania"><?= ($postmeta[$this->prefix_metadata.'company'][0] != "")?"(".$postmeta[$this->prefix_metadata.'company'][0].")":"" ?></span><span
         class="sala"><?= $postmeta[$this->prefix_metadata.'venue'][0] ?></span><span
         class="entradas"><?= do_shortcode($postmeta[$this->prefix_metadata.'tickets'][0]) ?></span><span
         class="social"><?= do_shortcode($postmeta[$this->prefix_metadata.'social'][0]) ?></span><span
         class="frase"><?= $postmeta[$this->prefix_metadata.'tagline'][0] ?></span></li>
      <?php
    endwhile;
    ?>
    </ul> 

    </div> 
    <?php 

    return ob_get_clean();
  }

  function show_calendar_mini($loop){

    $day = "";
    while ( $loop->have_posts() ) : $loop->the_post();   

      $t = get_post_time() - 3*3600; /* Hasta las 3AM es el mismo día */
      $tday  = date('dmy', $t);
      if($day != $tday) {
        if ($day !="")
          echo "</ul>";
        $day = $tday;
        ?> 
          <h3><?= date_i18n('l j F',$t); ?></h3>
          <ul>
       <?php
      }

      $postmeta = get_post_meta( get_the_ID() );
      //var_dump($postmeta);   
      ?>
      <li><span class="hora"><?php the_time('G:i\h'); ?></span>
        <span class="titulo"><?php
        if($postmeta[$this->prefix_metadata.'show_id'][0] != ''){
          ?><a href="<?= get_permalink( $postmeta[$this->prefix_metadata.'show_id'][0] ) ?>"><?php the_title(); ?></a><?php
        } else {
         the_title();
        }
        ?></span> <span
         class="compania"><?= ($postmeta[$this->prefix_metadata.'company'][0] != "")?"(".$postmeta[$this->prefix_metadata.'company'][0].")":"" ?></span> <span
         class="sala"><?= $postmeta[$this->prefix_metadata.'venue'][0] ?></span><span
         class="entradas"><?= do_shortcode($postmeta[$this->prefix_metadata.'tickets'][0]) ?></span>
        </li><?php

    endwhile;
    ?>
    </ul>
    <?php
  }

  function admin_calendar_URL($options = ""){
    $url = "edit.php?post_type=aomi_event&page=aomi_calendar";

    $opt['m'] = (isset($options['m']))?$options['m']:((isset($_GET['m']))?(int)$_GET['m']:date("m"));
    $opt['y'] = (isset($options['y']))?$options['y']:((isset($_GET['y']))?(int)$_GET['y']:date("Y"));

    if($opt['m'] > 12){
      $opt['m'] = 1;
      $opt['y'] += 1;
    }
    if($opt['m'] < 1){
      $opt['m'] = 12;
      $opt['y'] -= 1;
    }
/*
var_dump($opt);
var_dump($options);
var_dump($_GET);
*/
    foreach ($opt as $key => $value) {
      if($value !== NULL)
        $url .= "&$key=$value";
    }
    return $url;
  }

  function admin_calendar(){
    ?>
    <div class="wrap">
        <h1><?= esc_html(get_admin_page_title()); ?></h1>

        <?php
        $year = (isset($_GET['y']))?(int)$_GET['y']:date( 'Y' );
        $month = (isset($_GET['m']))?(int)$_GET['m']:date( 'm' );
        
        $args = array(
          'post_type' => 'aomi_event',
          'posts_per_page' => -1,
          'post_status' => 'any',
          
          'date_query' => array(
              array(
                'year' => $year,
                'month' => $month,
              ),
            ),

          'orderby' => 'date',
          'order'   => 'ASC',
          );
        $loop = new WP_Query( $args );


?>
      <div class="">
      <a href="<?=  $this->admin_calendar_URL(array('y'=>$year-1)) ?>">&lt;&lt;</a> <?= $year ?> <a href="<?= $this->admin_calendar_URL(array('y'=>$year+1))  ?>">&gt;&gt;</a><br/>
      <a href="<?=  $this->admin_calendar_URL(array('m'=>$month-1)) ?>">&lt;&lt;</a> <?= $month ?> <a href="<?= $this->admin_calendar_URL(array('m'=>$month+1))  ?>">&gt;&gt;</a><br/>
      </div>

      <table class="row">
        <tr>
<?php

        $day = "";
        $week = "";
        while ( $loop->have_posts() ) : $loop->the_post();   

          $t = get_post_time() - 3*3600; /* Hasta las 3AM es el mismo día */
          $event_day  = date('dmy', $t);

          $event_week = date('W', $t);
          if($week != $event_week) {
            if ($week !="")
              echo "</td>";
            $week = $event_week;
            echo "<td class='col-md-3' style='vertical-align:top';>";
          }


          if($day != $event_day) {
            if ($day !="")
              echo "</ul>";
            $day = $event_day;
            ?> 
              <h3><?= date_i18n('l j F',$t); ?></h3>
              <ul>
           <?php
          }

          $postmeta = get_post_meta( get_the_ID() );
          //var_dump($postmeta);   
          ?>
          <li><span class="hora"><?php the_time('G:i\h'); ?></span>
            <span class="titulo"><?= ($postmeta[$this->prefix_metadata.'show_id'][0] != '')?'('.$postmeta[$this->prefix_metadata.'show_id'][0].') ':'' ?><a href="<?= get_edit_post_link( get_the_ID() ) ?>" class="icon icon-edit"
              ><?php the_title(); ?></a></span><span
             class="compania"> <?= ($postmeta[$this->prefix_metadata.'company_id'][0] != "")?"[".$postmeta[$this->prefix_metadata.'company_id'][0]."]":"" ?><?= ($postmeta[$this->prefix_metadata.'company'][0] != "")?"(".$postmeta[$this->prefix_metadata.'company'][0].")":"" ?></span></li>
          <?php
        endwhile;
        ?>
        </ul>  
      </td>
    </tr>
  </table>


    </div>
    <?php
  }

}


$aomi_calendar = new AO_MadridImprovisa_Calendar();
