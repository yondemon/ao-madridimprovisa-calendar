<?php 

class aomi_calendarwidget extends WP_Widget {

  /**
   * Sets up the widgets name etc
   */
  public function __construct() {
    $widget_ops = array( 
      'classname' => 'aomi_calendarwidget',
      'description' => 'MI: Calendar widget',
    );
    parent::__construct( 'aomi_calendarwidget', 'Calendar Widget', $widget_ops );
  }

  /**
   * Outputs the content of the widget
   *
   * @param array $args
   * @param array $instance
   */
  public function widget( $args, $instance ) {
    global $aomi_calendar;

    $loop = $aomi_calendar->get_calendar_events();


    echo $args['before_widget'];

    echo $args['before_title'] . apply_filters( 'widget_title', "Agenda de hoy" ) . $args['after_title'];

    ?>
    <div class="agenda-widget">
    <?php
      $aomi_calendar->show_calendar_mini($loop);
    ?>
    </div> 
    <?php
    
    echo $args['after_widget'];
  }

  /**
   * Outputs the options form on admin
   *
   * @param array $instance The widget options
   */
  public function form( $instance ) {
    // outputs the options form on admin
    echo "FORM WIDGET";
  }

  /**
   * Processing widget options on save
   *
   * @param array $new_instance The new options
   * @param array $old_instance The previous options
   */
  public function update( $new_instance, $old_instance ) {
    // processes widget options to be saved
  }
}